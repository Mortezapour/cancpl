NEMO4 Modifications
===================
NEMO4 largely follows the same structure as CanESM5 with the following differences. These notes are based on the
NEMO 4.0.3 codebase.

- NEMO4 allows for domains with different shapes. The ``mppgather`` and ``mppscatter`` routines used to collect fields
  onto a global array had to be updated to support this.
- The eORCA1 grid has a very similar structure to the ORCA1 grid with the most significant changes being the landmask
  with a smaller, more realistic Caspian sea, some small changes in the coastlines. If the under ice-sheet cavities are
  excluded, then the grid has the same 362x292 dimensions as the ORCA1 grid
- In an attempt to more cleanly incorporate potential upstream changes to the coupling interface, a layer was written
  between ``sbccpl.F90`` and the OASIS coupler calls. The signature for CanCPL routines was also updated to match the
  abstract interface description based on the equivalent OASIS routines.
- XIOS can be run in 'detached' mode where a separate set of processors is devoted to I/O. CanCPL initialization and
  the definition of the colored communicators is so far a blocking call. XIOS may need to be modified to call
  ``define_group`` to enable support. OASIS seems to deal with this by allowing the coupler to return two communicators
  one for the actual NEMO processes and another for the XIOS processes. An extensive dive into how data are exchanged
  between XIOS and NEMO has not yet been done.
- One oddity that seems to be within the NEMO code is that if the land processor exclusion is enabled by setting ``jpni``,
  ``jpnj``, and ``jpnij``, some of areas of the ocean are incorrectly blocked out.
- Some consideration could be given for the atmosphere to calculate fluxes among the different ice thickness
  categories, requiring a 3D array of fluxes ``(jpcat,jpi,jpj)``. Some of this machinery appears to exist, but likely has
  not been tested recently.