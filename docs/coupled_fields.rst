Coupled Fields
==============

The definition of each coupled field can be found at the `CCCma TWiki <http://wiki.cccma.ec.gc.ca/twiki/bin/view/Main/CouplerTwoConfigCnsrv>`_.

For those outside of the CCCma, the raw TWiki code is included below:

.. literalinclude:: couplingtwiki.txt