program cpl_main
  !****************************************************************
  !--- Driver for the CCCma climate model coupler
  !****************************************************************
  use cpl_main_mod
  implicit none

  !===============================================================================
  !===============================================================================

  !--- Start MPI
  call mpi_init( ierr )

  !--- Initialize machine and intsize (required for CCCma io routines)
  machine = me32o64(idx)
  intsize = inteflt(idx)

  !--- Initialize total timer
  call wall_time("total")

  !--- Initialization timer
  call wall_time("start-up")

  !--- Initialize groups for cpl, atm, ocn, ice, ...
  !--- This will, among other things, define cpl_master, atm_master, ocn_master
  !--- NOTE: cpl_master is the rank in model_communicator not the rank in cpl_comm
  call define_group('cpl', cpl_comm)

  !--- read namelists and define the variable list
  call read_namelists_and_define_var_list()

  !--- Initialize the coupler event array and broadcast common variables
  !--- cpl_initialize_events will first call cpl_define_global_parameters to
  !--- broadcast values of certain atm and ocn variables to all mpi tasks
  !--- and then populate the global event array
  !--- e.g. atm_delt, atm_kstart, atm_ksteps, atm_kfinal, atm_kount
  !---      nemo_nn_ice, nemo_ncat, nemo_rn_rdt, nemo_nn_it000, nemo_nn_itend,
  !---      nemo_nn_date0, nemo_n_send_var, nemo_n_recv_var
  call cpl_initialize_events()

  !--- Determine rank and processor count in cpl_comm
  call mpi_comm_rank( cpl_comm, rank, ierr )
  call mpi_comm_size( cpl_comm, nproc, ierr )
  if (verbose > 2) then
    write(6,'(a,i12,3i8)')'=== cpl_main: cpl_comm, rank, size, cpl_master ', &
                       cpl_comm,rank,nproc,cpl_master
    flush(6)
  endif

  !--- Determine the rank of this task in model_communicator
  call mpi_comm_rank( model_communicator, mynode, ierr )

  !--- cpl_PetCount is used in the ccc_esmf module
  cpl_PetCount = nproc
  if (verbose > 2) then
    write(6,'(a,4i8)')'=== cpl_main: cpl_PetCount = ',cpl_PetCount
  endif

  !--- Initialize ESMF
  !--- This must be done after MPI is initialized
  !--- Pass cpl_comm as a default integer to avoid type mismatches when
  !--- integer word size changes via compiler options
  idx = cpl_comm
  call init_esmf(idx, "cpl.Log")

  !--- Initialize ESMF grid objects
  call init_grids()

  !--- Write runtime grid information to disk (these files are saved with the restart)
  call create_atm_grid_file("cpl_atm_grid.nc")
  call create_ocn_grid_file("cpl_ocn_grid.nc")

  !--- Allocate and assign the global event array
  !--- This must be done after ESMF is initialized and grids are defined
  call define_events()

  !--- Define run start and stop times from year/month data
  !--- present in the namelist main_coupler_par
  if ( env_run_start_year > -1 .and. env_run_start_month > -1 ) then
    !--- Note that yy, mm, dd, h, m, s must be assigned integer*4 values in this call
    start_year  = env_run_start_year
    start_month = env_run_start_month
    start_day   = 1
    call ESMF_TimeSet(env_run_start_time, yy=start_year, mm=start_month, dd=start_day, rc=rc_esmf)
    call handle_ESMF_err(rc_esmf, "Problem setting env start time in coupler")
    if ( verbose > -1 ) then
      call ESMF_TimeGet(env_run_start_time, timeStringISOFrac=time_string)
      write(6,'(2a)')"=== cpl_main: env_run_start_time: ",trim(time_string)
    endif
  endif

  if ( env_run_stop_year > -1 .and. env_run_stop_month > -1 ) then
    !--- Note that yy, mm, dd, h, m, s must be assigned integer*4 values in this call
    start_year  = env_run_stop_year
    start_month = env_run_stop_month
    start_day   = days_in_month(env_run_stop_month)
    call ESMF_TimeSet(env_run_stop_time, yy=start_year, mm=start_month, dd=start_day, rc=rc_esmf)
    call handle_ESMF_err(rc_esmf, "Problem setting env stop time in coupler")
    if ( verbose > -1 ) then
      call ESMF_TimeGet(env_run_stop_time, timeStringISOFrac=time_string)
      write(6,'(2a)')"=== cpl_main:  env_run_stop_time: ",trim(time_string)
    endif
  endif

  if ( len_trim(env_start) > 0 ) write(6,'(2a)')"=== cpl_main: env_start = ",trim(env_start)
  if ( len_trim(env_model) > 0 ) write(6,'(2a)')"=== cpl_main: env_model = ",trim(env_model)

  !--- Define the atm and ocn run time in model seconds
  !--- NOTE: nemo_nn_itend is the last time step in the NEMO run whereas
  !---       atm_kfinal is the last time step plus 1 in the AGCM run
  if ( atm_kfinal <= atm_kount ) then
    write(6,'(a)')'=== cpl_main: kfinal is less that or equal to kount.'
    write(6,'(a,i12)')'   kount = ',atm_kount
    write(6,'(a,i12)')'  kfinal = ',atm_kfinal
    call xit("cpl_main",-1)
  endif
  atm_run_time_secs = (atm_kfinal-atm_kount)*atm_delt

  if ( nemo_nn_itend < nemo_nn_it000 ) then
    write(6,'(a)')'=== cpl_main: nn_itend is less that nn_it000.'
    write(6,'(a,i12)')' nn_it000 = ',nemo_nn_it000
    write(6,'(a,i12)')' nn_itend = ',nemo_nn_itend
    call xit("cpl_main",-1)
  endif
  ocn_run_time_secs = (nemo_nn_itend - nemo_nn_it000 + 1) * nemo_rn_rdt
  if ( verbose > -5 ) then
    write(6,'(a,i10)')"=== cpl_main:    atm_kstart=",atm_kstart
    write(6,'(a,i10)')"=== cpl_main:    atm_ksteps=",atm_ksteps
    write(6,'(a,i10)')"=== cpl_main:     atm_kount=",atm_kount
    write(6,'(a,i10)')"=== cpl_main:    atm_kfinal=",atm_kfinal
    write(6,'(a,i10)')"=== cpl_main:      atm_delt=",atm_delt
    write(6,'(a,i12)')'=== cpl_main: nemo_nn_date0=',nemo_nn_date0
    write(6,'(a,i12)')'=== cpl_main: nemo_nn_it000=',nemo_nn_it000
    write(6,'(a,i12)')'=== cpl_main: nemo_nn_itend=',nemo_nn_itend
    write(6,'(a,i12)')'=== cpl_main:   nemo_rn_rdt=',nemo_rn_rdt
    write(6,'(a,i8,a)')"=== cpl_main: ATM run time is ",atm_run_time_secs," seconds."
    write(6,'(a,i8,a)')"=== cpl_main: OCN run time is ",ocn_run_time_secs," seconds."
    write(6,'(a,l1)')  '=== cpl_main:    atm_forcing_from_file=',atm_forcing_from_file
    write(6,'(a,i8)')  '=== cpl_main: specified_bc_year_offset=',specified_bc_year_offset
    flush(6)
  endif

  if ( use_nemo ) then
    if ( atm_run_time_secs /= ocn_run_time_secs ) then
      write(6,'(a)')'=== cpl_main: The ocean and the atmosphere have different run times.'
      write(6,'(a,i12,a)')'  ATM is set to run for ',atm_run_time_secs,' seconds'
      write(6,'(a,i12,a)')'  OCN is set to run for ',ocn_run_time_secs,' seconds'
      call xit("cpl_main",-1)
    endif
  endif

  if ( atm_run_time_secs < 1 ) then
    write(6,'(a,i12)')'=== cpl_main: Invalid total run time ',atm_run_time_secs
    call xit("cpl_main",-1)
  endif

  !--- Define the total run time as an ESMF interval variable
  cpl_run_time_secs = atm_run_time_secs
  call ESMF_TimeIntervalSet(cpl_total_time, ESMF_CALKIND_NOLEAP, s=cpl_run_time_secs, rc=rc_esmf)
  call handle_ESMF_err(rc_esmf, "Problem setting CPL run time")

  !--- Set the start_time for this invocation
  !--- The value of secs is seconds since 1-1-1 0:0:0 in the following
  secs = atm_kount*atm_delt + sec_per_year
  call secs2ymd(secs, ymd_hms)
  start_year  = ymd_hms(1)
  start_month = ymd_hms(2)
  start_day   = ymd_hms(3)
  start_hour  = ymd_hms(4)
  start_minute= ymd_hms(5)
  start_second= ymd_hms(6)
  !--- Note that yy, mm, dd, h, m, s must be assigned integer*4 values in this call
  call ESMF_TimeSet(start_time, yy=start_year, mm=start_month, dd=start_day, &
                                h=start_hour, m=start_minute, s=start_second, rc=rc_esmf)
  call handle_ESMF_err(rc_esmf, "Problem setting start time in coupler")

  !--- Set the coupler reference time for this invocation
  ref_time = start_time

  !--- Set the coupler start time for this invocation
  cpl_start_time = start_time

  !--- Initialize the string cpl_time_string (found in com_cpl) with the coupler starting time
  !--- and brodacast this value to all tasks
  call ESMF_TimeGet(cpl_start_time, timeStringISOFrac=cpl_time_string)
  do idx=1,len_trim(cpl_time_string)
    if ( cpl_time_string(idx:idx) .eq. "T" ) cpl_time_string(idx:idx) = " "
  enddo
  call bcastGroup(cpl_time_string, cpl_master, model_communicator)

  !--- Set the coupler stop time for this invocation
  cpl_stop_time = start_time + cpl_total_time

  !--- cpl_step is the coupler time step interval (in seconds)
  !--- This must be done after all events are defined (after the call to define_events)
  !--- since the coupling frequency of every event is required
  cpl_step = get_cpl_step(idx)
  if ( idx /= 0 ) then
    write(6,*)'=== cpl_main: Problem determining the coupler time step.'
    call xit("cpl_main",-1)
  endif
  write(6,'(a,i8,a)')"=== cpl_main: Coupler time step is ",cpl_step," seconds"
  call flush(6)

  !--- Assign this to a variable that is available to any routine that uses com_cpl
  cpl_time_step_secs = cpl_step
  cpl_time_step_iter = 0

  !--- Define the coupler time step interval as an ESMF interval variable
  call ESMF_TimeIntervalSet(time_step, ESMF_CALKIND_NOLEAP, s=cpl_step, rc=rc_esmf)
  call handle_ESMF_err(rc_esmf, "Problem setting minimum coupling interval in coupler")

  !--- Reset start time to be 1 coupler time step interval before the desired start time
  !--- This will allow the coupler clock to be incremented at the top of the time loop
  !--- Note that the alarm start time is not changed
  start_time = start_time - time_step

  !--- Assign the coupler start time in seconds
  !--- cpl_start_time_secs is found in com_cpl
  call ESMF_TimeGet(start_time, s_i8=cpl_start_time_secs)

  !--- Create the coupler clock
  cpl_clock = ESMF_ClockCreate(time_step, startTime=start_time, stopTime=cpl_stop_time, &
                               refTime=ref_time, name="Coupler Clock", rc=rc_esmf)
  call handle_ESMF_err(rc_esmf, "Problem creating coupler clock")

  call ESMF_TimeGet(cpl_start_time, timeStringISOFrac=time_string)
  write(6,'(2a)')"=== cpl_main: cpl_start_time: ",trim(time_string)
  call ESMF_TimeGet(cpl_stop_time, timeStringISOFrac=time_string)
  write(6,'(2a)')"=== cpl_main:  cpl_stop_time: ",trim(time_string)
  call flush(6)

  !--- Allocate and assign the global alarm array and assign alarms to events
  call set_alarms(cpl_clock, cpl_start_time)

  !--- Initialize ringing_alarm_count
  ringing_alarm_count = 0

  !--- Read the current time from the coupler clock
  time_string = get_time_string(cpl_clock)

  if ( len_trim(cpl_rs_file_name_in) < 1 ) then
    write(6,*)'cpl_main: Input restart file name is blank.'
    call xit("cpl_main",-2)
  endif

  !--- Ensure that the coupler restart file exists then read it
  inquire( file=trim(cpl_rs_file_name_in), exist=exists )
  if ( exists ) then
    write(6,*)'cpl_main: : ',trim(cpl_rs_file_name_in)," exists"
    call flush(6)
  else
    write(6,*)'cpl_main: : ',trim(cpl_rs_file_name_in)," does not exist"
    call flush(6)
  endif
  if ( .not. exists .and. &
       env_run_start_year == start_year .and. env_run_start_month == start_month ) then
    !--- If a coupler restart file does not exist then attempt to create one in certain cases
    !--- Do this only at the start of a run
    if ( cpl_use_initial_conditions .or. atm_forcing_from_file ) then
      !--- If there is no restart file then create one filled with initial conditions
      write(6,'(3a)')"=== cpl_main: Restart file ",trim(cpl_rs_file_name_in), &
          " does not exist. Creating a new coupler restart file."
      call restart_write(trim(cpl_rs_file_name_in), time_string, initial=.true.)
    else
      write(6,'(3a)')"=== cpl_main: Coupler restart file ",trim(cpl_rs_file_name_in)," is missing."
      call xit("cpl_main",-3)
    endif
  endif
  call restart_read(cpl_rs_file_name_in, time_string)
  write(6,*)'cpl_main: Time stamp found in restart file: ',trim(time_string)
  call flush(6)

  !--- Get the current time on the clock
  call ESMF_ClockGet(cpl_clock, currTime=curr_time, rc=rc_esmf)
  call handle_ESMF_err(rc_esmf, "cpl_main: Problem reading initial time.")

  !--- Write variables read from the coupler restart to the history file
  cpl_elapsed_time_secs = 0
  call history_write(cpl_hist_file_name, cpl_clock)

  call wall_time("start-up")

  !--- Loop over time
  TIME_LOOP: do while ( .true. )

    !--- Only the master node should do anything below this line in TIME_LOOP
    !??? if ( mynode /= cpl_master ) cycle TIME_LOOP

    !--- Keep track of the current iteration of this loop
    cpl_time_step_iter = cpl_time_step_iter + 1


    call wall_time("cpl time step", reset=.true.)

    !--- Advance the coupler clock
    call ESMF_ClockAdvance(cpl_clock, ringingAlarmCount=ringing_alarm_count, rc=rc_esmf)
    call handle_ESMF_err(rc_esmf, "Problem advancing time on coupler clock")

    !--- Get the current time on the clock
    call ESMF_ClockGet(cpl_clock, currTime=curr_time, rc=rc_esmf)
    call handle_ESMF_err(rc_esmf, "cpl_main: Problem reading time.")

    !--- Assign the string cpl_time_string (found in com_cpl) with the current coupler time
    call ESMF_TimeGet(curr_time, timeStringISOFrac=cpl_time_string)
    do idx=1,len_trim(cpl_time_string)
      if ( cpl_time_string(idx:idx) .eq. "T" ) cpl_time_string(idx:idx) = " "
    enddo

    !--- Assign the number of elasped seconds at the current coupler time
    !--- This will be seconds since 1 coupling interval before the start of month and therefore
    !--- will represent the end of the coupling interval (which corresponds with what the AGCM
    !--- thinks the time is when it sends fields back to the coupler)
    !--- cpl_elapsed_time_secs is found in com_cpl
    call ESMF_TimeGet(curr_time, s_i8=cpl_elapsed_time_secs)
    cpl_elapsed_time_secs = cpl_elapsed_time_secs - cpl_start_time_secs

    !--- Assign year and day of the year at the current coupler time
    call ESMF_TimeGet(curr_time, yy=cpl_year, dayOfYear=cpl_day_of_year)
    write(6,'(a,i3,4(a,i12))')"=== cpl_main: iter=",cpl_time_step_iter, &
                              " cpl_start_time_secs=",cpl_start_time_secs, &
                              " cpl_elapsed_time_secs=",cpl_elapsed_time_secs, &
                              " cpl_year=", cpl_year, &
                              " cpl_day_of_year=",cpl_day_of_year

    !--- Write current time info to stdout
    write(6,'(a,i4,3a,i10)')"=== cpl_main: iter=",cpl_time_step_iter, &
                            " current time: ",trim(cpl_time_string), &
                            " elasped time (seconds): ",cpl_elapsed_time_secs
    call flush(6)

    if ( ESMF_ClockIsStopTime(cpl_clock) ) then
      !--- This is the last time step
      !--- Send a message to the atm telling it to stop
      xbuf(:) = 0
      ibuf(1) = transfer("GRID", 1_8)
      ibuf(2) = 0
      ibuf(3) = transfer(" MSG", 1_8)
      ibuf(4) = 1
      ibuf(5) = 8
      ibuf(6) = 1
      ibuf(7) = 0
      ibuf(8) = 1
      write(6,*)"=== cpl_main: Stop Time: xbuf = ",xbuf(1:8)
      call flush(6)
      call send_data_rec(xbuf, ibuf, atm_master, "MSG", dbg=1)

      !--- Exit the time loop
      exit TIME_LOOP
    endif

    call wall_time("process alarms", reset=.true.)
    !--- Process any ringing alarms
    if (ringing_alarm_count > 0) then
      do idx=1,nevents
        if ( ESMF_AlarmIsRinging( alarm(event(idx)%alarm_index), rc=rc_esmf) ) then
          !--- Take any action required by this alarm
          call execute_event(idx, cpl_clock, evrc)

          !--- Turn alarm off after processing
          call ESMF_AlarmRingerOff( alarm(event(idx)%alarm_index), rc=rc_esmf)

          !--- Process the return code from execute_event
          if ( evrc /= 0 ) then
            !--- A return code of 1 means a normal exit
            if ( evrc == 1 ) exit TIME_LOOP

            !--- Any other non-zero value is an error
            write(6,*)"=== cpl_main: Error in event handler ",evrc
            call xit("cpl_main",-90)
          endif
        endif
      enddo
    endif
    call wall_time("process alarms", verbose=0)

    !--- Note: atm_kount2 is sent from the AGCM after it has integrated for 1 coupling time step,
    !--- written to the gs file and sent flux fields back to the coupler. This means that the AGCM
    !--- is at the end of its coupling cycle and therefore the value of atm_kount2 reflects this.
    !--- The value of secs is seconds since 1-1-1 0:0:0 in the following to be consistent with
    !--- the meaning of KOUNT in the AGCM
    secs = atm_kount2*atm_delt + sec_per_year
    call secs2ymd(secs, ymd_hms)
    i4year  = ymd_hms(1)
    i4month = ymd_hms(2)
    i4day   = ymd_hms(3)
    i4hour  = ymd_hms(4)
    i4minute= ymd_hms(5)
    i4second= ymd_hms(6)
    !--- Note that yy, mm, dd, h, m, s must be assigned integer*4 values in this call
    call ESMF_TimeSet(atm_time, yy=i4year, mm=i4month, dd=i4day, &
                                 h=i4hour, m=i4minute, s=i4second, rc=rc_esmf)
    call handle_ESMF_err(rc_esmf, "Problem setting atm time in coupler")

    call ESMF_TimeGet(atm_time, timeStringISOFrac=time_string)
    call FilterTS(time_string)

    write(6,'(a,i4,4a,a,i10)') &
        "=== cpl_main: iter=",cpl_time_step_iter, &
        " coupler time: ",trim(cpl_time_string), &
        "     atm time: ",trim(time_string), &
        "  atm_kount =",atm_kount2

    if ( write_cpl_output ) then
       call wall_time("write history", reset=.true.)
       call history_write(cpl_hist_file_name, cpl_clock)
       call wall_time("write history", verbose=0)
       call wall_time("cpl time step", verbose=0)
    endif

  enddo TIME_LOOP

  !--- Read the current time from the coupler clock
  time_string = get_time_string(cpl_clock)

  !--- Write the coupler restart file
  call restart_write(cpl_rs_file_name_out, time_string)

  !--- Get the elapsed wall time
  call wall_time("total", verbose=0)

  !--- Print timer stats
  call wall_time_stats(save_to_datapath=.false., verbose=1)

  if (verbose > 0) then
    write(6,'(a,i3,a)')'=== cpl_main: Coupler on node ',mynode,' is DONE.'
    call flush(6)
  endif

  !--- Stop MPI
  call mpi_finalize( ierr )

!---FIXME---
!--- The following call to ESMF_Finalize results in a segfault
!--- Not sure it is even necessary (could be for closing log files) since the code runs
!--- without this call so simply comment it out for now. Possibly get back to it later.
  !--- Stop ESMF
  !--- ESMF_Finalize must be called after the call to mpi_finalize since it will call
  !--- mpi_finalize if MPI is still active. If ESMF calls mpi_finalize then it will
  !--- only stop a subset of tasks as it is only aware of the coupler comm world.
!  call ESMF_Finalize( endflag=ESMF_END_KEEPMPI, rc=rc_esmf )
!  call handle_ESMF_err(rc_esmf, "cpl_main: Problem in ESMF_Finalize.")

end program cpl_main
